#!/usr/bin/env sh
dependencies="gpg cut test curl grep"
#~ if ! "$1" = "run"; then
#~ printf "stilly buggy!\n" ; exit 0;
#~ fi

#~ wget -q -O- https://api.bintray.com/orgs/gopasspw/keys/gpg/public.key | sudo apt-key add -
#~ echo "deb https://dl.bintray.com/gopasspw/gopass buster main" | sudo tee /etc/apt/sources.list.d/gopass.list

# todo: get files first and determine the signature key to compare
gopass_fpr="C21C8CAD294D35BF5A3BBB15B3C5B1A0560D8522"

echo "check for PGP key based on fingerprint "${gopass_fpr}
gpg -q --keyserver hkps://keyserver.ubuntu.com --recv-keys ${gopass_fpr} || { echo "error retreiving pgp public key"; exit 1; }

echo "collecting system information"
arch=$(uname --machine)
if test "${arch}" = "x86_64"; then
	arch="amd64"
fi
# get recent release version
uname=$(uname -a)
echo $uname|grep -i "debian" >/dev/null && { fn_suffix="linux_${arch}.deb"; }
echo $uname|grep -i "openbsd" >/dev/null && { fn_suffix="openbsd_${arch}.tar.gz"; }
echo $uname|grep -i "freebsd" >/dev/null && { fn_suffix="freebsd_${arch}.tar.gz"; }
test -z $fn_suffix && { echo "operating system not supported."; exit 1; }
#~ echo "fn_suffix: "${fn_suffix}

echo "check online resources"
url_effective=$(curl -sLI -o /dev/null -w %{url_effective} https://github.com/gopasspw/gopass/releases/latest)
url=$(echo ${url_effective} | sed 's/\/tag\//\/download\//')"/"
ver=$(echo -n $url | cut -d'/' -f8 | cut -d"v" -f2)

# https://github.com/gopasspw/gopass/releases/download/v1.14.9/gopass_1.14.9_linux_amd64.deb
fn1="gopass"
fn_package=${fn1}"_"${ver}"_"${fn_suffix}
fn_SHA256=${fn1}"_"${ver}"_SHA256SUMS"

# download files: deb and checksums
echo "downloading files"
todo: iterate over file list
#~ fnlist="${fn_package} ${fn_SHA256} ${fn_SHA256}.sig"
#~ for ${fn} in ${fnlist}; do
#~   curl -s --progress-bar -L $url$fn -o $fn || { echo "download failed"; exit 1; }
#~ done
curl -s --progress-bar -L $url$fn_package -o $fn_package || { echo "download failed"; exit 1; }
curl -s --progress-bar -L $url$fn_SHA256 -o $fn_SHA256 || { echo "download failed"; exit 1; }
curl -s --progress-bar -L $url$fn_SHA256".sig" -o $fn_SHA256".sig" || { echo "download failed"; exit 1; }

test $(du $fn_package|cut -f1) -gt 1024 || { echo "manually check small file before installation."; exit 1; }
# todo: get keyid / fpr from sig file
# keyid
#~ gpg --list-packets gopass_1.15.15_SHA256SUMS.sig |grep -i "keyid"|rev|cut -d" " -f1|rev
# fpr
#~ gpg --list-packets gopass_1.15.15_SHA256SUMS.sig |grep -i "fpr"|rev|cut -d" " -f1|rev|cut -d")" -f1
gpg -q --verify "${fn_SHA256}.sig" || { echo "verification for signature failed."; exit 2; } 


# test checksums
shahash="$(sha256sum $fn_package)"
cat "${fn_SHA256}" |grep "${shahash}" >/dev/null && {
	# test filesize, didnt see any near 10 MB
	test $(du $fn_SHA256|cut -f1) -lt 10240 || { echo "manually check exceptionally large file before installation."; exit 1; }
	# maybe install
	case $(echo $fn_suffix|cut -d"_" -f1) in
		linux)
			echo "installing with dpkg"
			sudo dpkg -i "${fn_package}"
			break
		;;
		*) 
			echo "please install manually"
		;;
	esac
} || {
	echo "checksum failed"#; exit 1
}
