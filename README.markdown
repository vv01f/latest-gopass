# gopass download and install script

As I cherish the tool but my distro does not provide a recent version,
this script shall download and install the latest release of gopass.

simply run `./get.sh` or maybe go through it step by step to verify.

There is [gopassbridge] for integration with chrome and firefox browsers.

[gopassbridge]: https://github.com/gopasspw/gopassbridge
